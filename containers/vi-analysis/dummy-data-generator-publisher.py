import numpy as np
import time
import paho.mqtt.client as mqtt
import json
import datetime


# Configs
TOPIC_MAX = "viAnalysis/max"
TOPIC_MIN = "viAnalysis/min"
TOPIC_MEAN = "viAnalysis/mean"

# MQTT callback functions

# The callback for when the client receives a CONNACK response from the server.
def on_connect(client, userdata, flags, rc):
    print("Connected flags ",str(flags),"result code ",str(rc))


    # Subscribing in on_connect() means that if we lose the connection and
    # reconnect then subscriptions will be renewed.
    client.subscribe("$SYS/#")




def message_constructor(value, topic, id):
    topic_str = topic.split("/")[1]
    message = {
        "id": topic_str + str(id),
        "value": str(value),
        "container": "viAnalysis",
        "timestamp": str(datetime.datetime.now())
    }

    message_json = json.dumps(message)
    return message_json

# Main
print("--- viAnalysis Publisher ---", flush=True)

# Wait for the broker to start
time.sleep(2)

client = mqtt.Client("viAnalysis-publisher", reconnect_on_failure=True)


client.username_pw_set(username="viAnalysis-publisher", password="root")

try:
    client.connect("central", 1883)
except Exception as e:
    print("PUB-ERROR", e, flush=True)

client.on_connect = on_connect



id = 0

while True:
    # Generates 1000 random values from a normal distribution with a mean of 500 and a spread of 250
    r = np.random.default_rng().normal(500, 250, 1000)

    max = abs(r.max())
    min = abs(r.min())
    mean = abs(r.mean())

    # Publisher
    client.publish(TOPIC_MAX, message_constructor(max, TOPIC_MAX, id))
    # print("Just published " + str(max) + " to Topic " + TOPIC_MAX, flush=True)

    client.publish(TOPIC_MIN, message_constructor(min, TOPIC_MIN, id))
    # print("Just published " + str(min) + " to Topic " + TOPIC_MIN, flush=True)

    client.publish(TOPIC_MEAN, message_constructor(mean, TOPIC_MEAN, id))
    # print("Just published " + str(mean) + " to Topic " + TOPIC_MEAN, flush=True)

    id = id + 1
    time.sleep(3)