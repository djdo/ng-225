import paho.mqtt.client as mqtt
import json
import time

# Configs
TOPIC_MAX = "viAnalysis/max"
TOPIC_MIN = "viAnalysis/min"
TOPIC_MEAN = "viAnalysis/mean"

def on_connect(client, userdata, flags, rc, properties=None):
    print("Connected flags ",str(userdata),"result code ",str(rc), flush=True)



def on_message(mqttc, obj, msg):
    message_json = msg.payload
    message = json.loads(message_json)
    print(msg.topic + " : " + str(msg.qos) + " : " + str(message), flush=True)


def on_publish(mqttc, obj, mid):
    print("mid: " + str(mid))


def on_subscribe(mqttc, obj, mid, granted_qos):
    print("Subscribed: " + str(mid) + " " + str(granted_qos))


def on_log(mqttc, obj, level, string):
    print(string)


# Main
print("--- viAnalysis Subscriber ---", flush=True)

# Wait for the broker to start
time.sleep(2)

mqttc = mqtt.Client("viAnalysis-subscriber", reconnect_on_failure=True)
mqttc.on_message = on_message
mqttc.on_connect = on_connect
mqttc.on_publish = on_publish
mqttc.on_subscribe = on_subscribe


mqttc.username_pw_set(username="viAnalysis-subscriber", password="root")


# Uncomment to enable debug messages
# mqttc.on_log = on_log

try:
    mqttc.connect("central", 1883, 60)
except Exception as e:
    print("SUB-ERROR", e, flush=True)

mqttc.subscribe(TOPIC_MAX, 0)
mqttc.subscribe(TOPIC_MIN, 0)
mqttc.subscribe(TOPIC_MEAN, 0)

mqttc.loop_forever()