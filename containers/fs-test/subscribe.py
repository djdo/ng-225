import paho.mqtt.client as mqtt
import time

print("--- FS Subscriber ---", flush=True)

time.sleep(10)

# The callback for when the client receives a CONNACK response from the server.
def on_connect(client, userdata, flags, rc,  properties=None):
    print("Connected with result code "+str(rc), flush=True)

    # Subscribing in on_connect() means that if we lose the connection and
    # reconnect then subscriptions will be renewed.
    # client.subscribe("$SYS/#")
    client.subscribe("/configs/test/test2.json", 1)
    client.subscribe("/configs/test/test1.json", 1)
    client.subscribe("/configs/test/subtest/subtest1.json", 1)
    client.subscribe("/configs/test/subtest2/subtest3.json", 1)
    client.subscribe("/configs/test/subtest2/subtest2.json", 1)

# The callback for when a PUBLISH message is received from the server.
def on_message(client, userdata, msg):
    print(msg.topic+" "+str(msg.payload), flush=True)

def on_subscribe(mqttc, obj, mid, granted_qos, properties=None):
    print("Subscribed: "+str(mid)+" "+str(granted_qos), flush=True)

def on_log(mqttc, obj, level, string):
    print(string, flush=True)

client = mqtt.Client("fs-subscriber", reconnect_on_failure=True)

client.username_pw_set(username="fs-subscriber", password="root")

client.on_connect = on_connect
client.on_message = on_message
client.on_subscribe = on_subscribe
# client.on_log = on_log

client.connect("central", 1883)

# client.subscribe("/configs/test/test2.json", 1)
# client.subscribe("/configs/test/test1.json", 1)
# client.subscribe("/configs/test/subtest/subtest1.json", 1)
# client.subscribe("/configs/test/subtest2/subtest3.json", 1)
# client.subscribe("/configs/test/subtest2/subtest2.json", 1)

client.loop_forever()