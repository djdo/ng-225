# Central container overview

## nvs-DB Save & Buffering App

* Stores the json files to database
* These files, at this stage, are settings files

## Configuration Management App

* To be developed later

## Application Data Callbacks

* To be developed later
* Makes sure every module gets the message from the broker, even if it down or something has gone wrong

## Mosquitto-broker

* Self explanatory

## Mosquitto supervisor

* To be developed later

# To Do

* Generalize publish-configs-to-broker.py to accept parameters such as an array of files, QoS and retain values.
* Develop a script that stores the json settings files to the database 
* Decide on a json settings file template

# Main points of demo presentation

Main achievements during sprint
Risks identified, constraints or other urgent subjects
Sprint Retrospective