#!/bin/sh

# Which cipher to use?
CIPHER="-aes-128-cbc"

openssl genrsa $CIPHER -passout pass:root -out ca.key 2048