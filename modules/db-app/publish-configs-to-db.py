# https://kb.objectrocket.com/mongo-db/use-docker-and-python-for-a-mongodb-application-1046

import pymongo
import time
import sys
import glob
import json

start_time = time.time()

if len(sys.argv) < 2:
    print("Usage: python3 publish-configs-to-db.py <root_directory> [excluded_directories]")

root_dir = str(sys.argv[1])

excluded_directories = [str(x) for x in sys.argv[2:]]

client = pymongo.MongoClient("mongo", 27017, username='root', password='test')

db = client.system

modules_collections = db.settings

files = []

for filename in glob.iglob(root_dir + '**/*.json', recursive=True):
    for excluded_directory in excluded_directories:
        if filename.startswith(root_dir + excluded_directory):
            break
    else:
        with open(filename, 'r') as f:  
            f_json = json.load(f)
            files.append(f_json)
        print(filename)

modules_collections.insert_many(files)

        # try:
        #     collection = db.create_collection(name=(filename.removeprefix(root_dir)))
        # except:
        #     print("Failed to create collection: Already exists")
        # else:
        #     print(filename)
        #     modules_collections.append(collection)


# for filename in glob.iglob(root_dir + '**/*.*', recursive=True):
#     for excluded_directory in excluded_directories:
#         if filename.startswith(root_dir + excluded_directory):
#             break
#     else:
#         files.append(filename)
#         modules.append(glob.iglob(root_dir + '*'))


print(files)
print(client.list_database_names())
print(db.list_collection_names())

print(modules_collections.find())

# for module in modules:
#     modules_collections.append(db.create_collection(name=module))


# for file in files:
#     with open(file, 'r') as f:
#         f_json = json.load(f)


# x = collection.insert_many(customers_list)

# print(x.inserted_ids)

# print(client.list_database_names())

print(time.time() - start_time, "seconds")