import glob
import paho.mqtt.publish as publish
import sys
import json
import time

start_time = time.time()

if len(sys.argv) < 2:
    print("Usage: python3 publish-configs-to-broker.py <root_directory> [excluded_directories]")

root_dir = str(sys.argv[1])

excluded_directories = [str(x) for x in sys.argv[2:]]

def json_config_parser(jsonConfig, msgList, pathName):
    if len(jsonConfig) == 0:
        return msgList
    
    else:
        for i in jsonConfig:
            key = i[0]
            val = i[1]
            msgList.append({"topic" : pathName + "/" + key, "payload" : str(val), "qos" : 1, "retain" : True})
            # print(pathName + "/" + key + " added to message queue", flush=True)
            print(pathName + "/" + key + " added to message queue")

            if type(val) is not dict:
                msgList.append({"topic" : pathName + "/" + key, "payload" : str(val), "qos" : 1, "retain" : True})
            else:
                json_config_parser(val.items(), msgList, pathName + "/" + key)
        return msgList

# print("--- FS Publisher ---", flush=True)
print("--- FS Publisher ---")


# Messages, in this case files, to be published on the broker
msgs = []

# root_dir needs a trailing slash (i.e. /root/dir/)

files = []

for filename in glob.iglob(root_dir + '**/*.*', recursive=True):
    for excluded_directory in excluded_directories:
        if filename.startswith(root_dir + excluded_directory):
            break
    else:
        files.append(filename)

for file in files:
    with open(file, 'r') as f:
        f_json = json.load(f)
        json_config_parser(f_json.items(), msgs, str(file).replace('.json', ''))
        # print(file + " added to message queue", flush=True)
        print(file + " added to message queue")
        msgs.append({"topic": file, "payload": str(f_json), "qos" : 1, "retain" : True})
        f.close()

# for filename in glob.iglob(root_dir + '**/*.*', recursive=True):
#     for excluded_directory in excluded_directories:
#         print(root_dir + excluded_directory)
#         if filename.startswith(root_dir + excluded_directory):
#             break
#     with open(filename, 'r') as f:
#         f_json = json.load(f)
#         json_config_parser(f_json.items(), msgs, str(filename).replace('.json', ''))
#         # print(filename + " added to message queue", flush=True)
#         print(filename + " added to message queue")
#         msgs.append({"topic": filename, "payload": str(f_json), "qos" : 1, "retain" : True})
#         f.close()


# print(msgs)
publish.multiple(msgs, hostname="mqtt", port=1883, client_id="fs-publisher")

print(time.time() - start_time, "seconds")