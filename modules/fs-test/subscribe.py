import paho.mqtt.client as mqtt

print("--- FS Subscriber ---", flush=True)


# The callback for when the client receives a CONNACK response from the server.
def on_connect(client, userdata, flags, rc,  properties=None):
    print("Connected with result code "+str(rc))

    # Subscribing in on_connect() means that if we lose the connection and
    # reconnect then subscriptions will be renewed.
    client.subscribe("$SYS/#")

# The callback for when a PUBLISH message is received from the server.
def on_message(client, userdata, msg):
    print(msg.topic+" "+str(msg.payload))

def on_subscribe(mqttc, obj, mid, granted_qos, properties=None):
    print("Subscribed: "+str(mid)+" "+str(granted_qos))

def on_log(mqttc, obj, level, string):
    print(string)

client = mqtt.Client("fs-subscriber", protocol=mqtt.MQTTv5)
client.on_connect = on_connect
client.on_message = on_message
client.on_subscribe = on_subscribe
# client.on_log = on_log

client.connect("mqtt", 1883)

client.subscribe("/configs/test/test2.json", 1)
client.subscribe("/configs/test/test1.json", 1)
client.subscribe("/configs/test/subtest/subtest1.json", 1)
client.subscribe("/configs/test/subtest2/subtest3.json", 1)
client.subscribe("/configs/test/subtest2/subtest2.json")

client.loop_forever()